#!/bin/bash
# Script to assist with installing whonix on kvm
#
sudo apt-get install --no-install-recommends qemu-kvm libvirt-daemon-system libvirt-clients virt-manager gir1.2-spiceclientgtk-3.0 dnsmasq qemu-utils
sudo addgroup "$(whoami)" libvirt
sudo addgroup "$(whoami)" kvm
sudo reboot now

