#!/bin/bash
# Script to assist with installing whonix on kvm
#
sudo virsh -c qemu:///system net-autostart default
sudo virsh -c qemu:///system net-start default
tar xvf Whonix-XFCE-16.0.2.7.Intel_AMD64.qcow2.libvirt.xz
touch WHONIX_BINARY_LICENSE_AGREEMENT_accepted
sudo virsh -c qemu:///system net-define Whonix_external*.xml
sudo virsh -c qemu:///system net-define Whonix_internal*.xml
sudo virsh -c qemu:///system net-autostart Whonix-External
sudo virsh -c qemu:///system net-start Whonix-External
sudo virsh -c qemu:///system net-autostart Whonix-Internal
sudo virsh -c qemu:///system net-start Whonix-Internal
sudo virsh -c qemu:///system define Whonix-Gateway*.xml
sudo virsh -c qemu:///system define Whonix-Workstation*.xml
sudo mv Whonix-Gateway*.qcow2 /var/lib/libvirt/images/Whonix-Gateway.qcow2
sudo mv Whonix-Workstation*.qcow2 /var/lib/libvirt/images/Whonix-Workstation.qcow2

